[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687546.svg)](https://doi.org/10.5281/zenodo.4687546)

# Renewable energy sources data collection and potential review - Shallow geothermal potential

The present task provides data with following characteristics:

<table>
  <tr>
    <td> </td>
    <td>Spatial resolution</td>
    <td>Temporal resolution</td>
  </tr>
  <tr>
    <td>Shallow geothermal potential</td>
    <td>Vector</td>
    <td>-</td>
  </tr>
</table>

**Table 1.** Characteristics of data provided within Task 2.6 Renewable energy sources data collection and potential review.

In this task, we collected and re-elaborated data on energy potential of renewable sources at national level, in order to build datasets for all EU28 countries at NUTS3 level. We considered the following renewable sources: biomass, waste and wastewater, shallow geothermal, wind, and solar energy. These data will be used in the toolbox to map the sources of renewable thermal energy across the EU28 and support energy planning and policy.

## Geothermal energy

Data on very shallow geothermal energy potential in W/m K were retrieved from the EC co-funded project ThermoMap as a vector layer and presented here without further elaboration. [150].

## Limitations of data

The data here calculated are estimations of the energy potential from renewable energy sources. The hypotheses we made when deciding what data to consider, when re-elaborating the data at more aggregated territorial levels and finally when deciding how to convey the results, can influence the results.

In some cases, we underestimated the actual potential (biomass), by downscaling the available resource for sustainability reasons, in others, we overestimated the potential (wind, solar) due to our assumption of using all available areas, according only to some GIS sustainable criteria, where energy generation is feasible without considering economic profitability.

The potentials here reported **do not account for any type of energy conversion**: when estimating the actual potential, the user will need to choose the technology through which the potential can be exploited (for example COP for the wastewater treatment plant or the efficiency for solar thermal, photovoltaic and wind).

For these reasons, the data must be considered **as indicators, rather than absolute figures** representing the actual energy potential of renewable sources in a territory.

## Clone reposotory

The reposipotory implements git-lfs. In order to clone the data you need to install [git-lfs](https://github.com/git-lfs/git-lfs/wiki/Installation) on your local machine.

## How to cite

Cite Thermomap project.
